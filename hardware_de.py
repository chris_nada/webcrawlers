import requests

debug = False # Nur 1 Produkt crawlen

##################################################################
### KONFIGURATION

# UserBenchmark
#https://www.userbenchmark.com/page/developer
#csv_cpu = "https://www.userbenchmark.com/resources/download/csv/CPU_UserBenchmarks.csv"
#csv_gpu = "https://www.userbenchmark.com/resources/download/csv/GPU_UserBenchmarks.csv"
#csv_ram = "https://www.userbenchmark.com/resources/download/csv/RAM_UserBenchmarks.csv"
csv_cpu = "CPU_UserBenchmarks.csv"
csv_gpu = "GPU_UserBenchmarks.csv"

# Alternate
url_alter = "https://www.alternate.de"
url_cpu   = "https://www.alternate.de/PC-Komponenten/CPUs/html/listings/1458213741910?lk=8521&size=500&hideFilter=false"
url_gpu   = "https://www.alternate.de/Grafikkarten/html/listings/1458222035317/1486466143032?lk=8378&size=500&hideFilter=false"

##################################################################

# Init
session = requests.Session()
adapter = requests.adapters.HTTPAdapter(max_retries=3)
session.mount('https://', adapter)
session.mount('http://', adapter)

# Extrahiert Alle Produkt URLS
def get_produkt_urls(url_liste):
	urls = []
	response = str(session.get(url_liste).content)
	for line in response.split("\n"):
		for tag in line.split("<"): 
			
			# Produktlink finden
			if "a " in tag and "/product/" in tag and "productLink" in tag:
				#print ("\n\nProdukt gefunden:")
				link = tag[tag.find("href=")+6:tag.rfind("\"")-1]
				urls.append(url_alter + link)
	return urls

# Extrahiert Info
def extract_name_preis(urls):
	namen = []
	preise = []
	for i in range(len(urls)):
		print ("Extrahiere", i+1, "/", len(urls))
		url_produkt = urls[i]
		produkt_name = ""
		produkt_preis = ""
		if debug:
			print ("\n\nExtrahiere aus " + url_produkt)
		response = str(session.get(url_produkt).content)
		for line in response.split("\n"):
			for tag in line.split("<"):
				if produkt_preis == "" and "data-standard-price" in tag:
					tags = tag.split(" ")
					for tag in tags:
						if "data-standard-price=" in tag:
							produkt_preis = tag[tag.find("\"")+1:tag.rfind("\"")]
							break
					continue
				if produkt_name == "" and "meta itemprop=\"name\"" in tag:
					produkt_name = tag[tag.find("content=")+9:tag.rfind("\"")]
					produkt_name = produkt_name.replace(",", "")
					produkt_name = produkt_name.replace(";", "") # , und ; verboten wg. CSV
					produkt_name = produkt_name[:produkt_name.find("(")] # bis zur (
					print ("\tPN:", produkt_name)
					continue
		
		# Daten ok? => Anfügen	
		if produkt_name != "" and produkt_preis != "":
			if debug:
				print (produkt_name + " : " + produkt_preis)
			namen.append(produkt_name)
			preise.append(produkt_preis)
			if debug:
				break
		else:
			if debug:
				print ("Name/Preis unvollständig:", produkt_name, ",", produkt_preis)
				
	return  [namen, preise]

def merge_data(namen_preise, csv, csv_out, score = 1.0):
	print ("merge_data(...) ...")
	out = [] # Zeilen zu schreiben
	with open(csv) as f:
		out.append("Q_Name,Q_Preis," + f.readline())
	
	# Jedes Name / Preis paar zuordnen
	for i in range(len(namen_preise[0])):
		gefunden = False
		name = namen_preise[0][i]
		preis = namen_preise[1][i]
		#print (name)
		#print (preis)
		namen = []
		name = name.replace("-", " ")
		for n in name.split(" "):
			n = n.lower()
			if len(n) > 1 and not "essor" in n and not "oxed" in n and not "(" in n and not ")" in n and not "&" in n:
				if "karte" not in n:
					namen.append(n)
		if debug:
			print ("Namen:", namen)
		
		# In CSV finden
		with open(csv) as f:
			lines = f.readlines()
			for line in lines:
				line = line.lower()
				tokens_found = 0
				for token in namen:
					if token in line:
						tokens_found += 1
				if tokens_found / len(namen) >= score:
					line = name + "," + preis + "," + line # Daten hinzufügen
					out.append(line)
					gefunden = True
					break
			if not gefunden:
				print ("Nicht gefunden in CSV:", name)
				
	if debug:
		print ("CSV Out:", out)
	with open(csv_out, "a") as f:
		for line in out:
			f.write(line)

# CPUs
urls = get_produkt_urls(url_cpu)
namen_preise = extract_name_preis(urls)
merge_data(namen_preise, csv_cpu, "cpus.csv")

# GPUs
urls = get_produkt_urls(url_gpu)
namen_preise = extract_name_preis(urls)
merge_data(namen_preise, csv_gpu, "gpus.csv", 0.75)

print("Done.")
